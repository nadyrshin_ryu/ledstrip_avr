//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <delay.h>
#include <ledstrip.h>
#include <examples.h>
#include <rgbw.h>


// ���-�� ������ ������� �������
#define Test_cycles             1 * RGBW_TST_STEP_NUM
#define Firework_cycles         10 * RGBW_FW_STEP_NUM
#define Rainbow_cycles          6
#define Stars_cycles            30


#if (LEDSTRIP_COLORSNUM == 4)
//==============================================================================
// ��������� ����������� �������� ��� 4-������� �����������
//==============================================================================
void rgbw_effects(void)
{
  uint16_t step, cycles, ticks;
  uint8_t fader;
  
  //// �������� ���� �����������
  cycles = Test_cycles;
  step = 0;
  while (cycles--)
  {
    ticks = rgbw_test_start(step);
    while (ticks--)
    {    
      int16_t delay = rgbw_test_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else        
        delay_ms(delay);
    }
    if (++step == RGBW_TST_STEP_NUM)
      step = 0;
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }
  
  //// �������� �����
  cycles = Firework_cycles;
  step = 0;
  while (cycles--)
  {
    ticks = rgbw_firework_start(step);
    while (ticks--)
    {    
      int16_t delay = rgbw_firework_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else        
        delay_ms(delay);
    }
    if (++step == RGBW_FW_STEP_NUM)
      step = 0;
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }
  
  //// �������� ������
  cycles = Rainbow_cycles;
  while (cycles--)
  {
    ticks = rgbw_rainbow_start(step);
    while (ticks--)
    {    
      int16_t delay = rgbw_rainbow_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else
        delay_ms(delay);
    }
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }

  //// �������� �¨���
  cycles = Stars_cycles;
  while (cycles--)
  {
    ticks = rgbw_stars_start(step);
    while (ticks--)
    {    
      int16_t delay = rgbw_stars_tick(step);
      ledstrip_update();
      
      if (delay < 0)
        ticks = 0;
      else
        delay_ms(delay);
    }
  }
  
  // ������� ���������� ���� �����������
  fader = 255;
  while (fader--)
  {
    ledstrip_fade_out_all(FADER_STEP);
    ledstrip_update();
    delay_ms(FADER_PERIOD);
  }
  
}
//==============================================================================
#endif