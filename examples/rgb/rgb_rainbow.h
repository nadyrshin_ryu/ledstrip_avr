//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RGB_RAINBOW_H
#define _RGB_RAINBOW_H

#include "..\types.h"


//// ��������� �������� ������
// ���-�� ����� �� ������� ����� 2 ������� ������
#define RGB_RB_1COLOR_STEPS    32
// ������ ���� �� �������� �� ������ ����� � �������
#define RGB_RB_FADE_STEP       8
// ������ ����� ������ � ��
#define RGB_RB_PERIOD          30


uint16_t rgb_rainbow_start(uint8_t step);
int16_t rgb_rainbow_tick(uint8_t step);

#endif