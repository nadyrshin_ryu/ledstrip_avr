//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include "examples.h"
#include "rgb_firework.h"
#include <stdlib.h>

#if (LEDSTRIP_COLORSNUM == 3)

// �������� ������ ����������
static uint16_t var1, var2, var3;
static uint16_t DotPos;
static tRGB color1;
     
//==============================================================================
// ������� �������� ����� ������. ���������� ���-�� ������ � ����� �������.
//==============================================================================
uint16_t rgb_firework_start(uint8_t step)
{
  var1 = var2 = var3 = 0;
  
  switch (step)
  {
  // ����� ������� �����
  case 0:
    // ��������� ������� ������� �����
    DotPos = 0;
    
    // ������ ����� ������ ������� �����
    var2 = (LEDSTRIP_LEDNUM / 4) + (rand() % (LEDSTRIP_LEDNUM - RGB_FW_FLASH_RADIUS - (LEDSTRIP_LEDNUM / 4)));
    
    // �������� ��������� ����
    color1.R = 0; color1.G = 0; color1.B = 0; 
    switch (rand() % 6)
    {
      case 0: 
        color1.R = RGB_FW_DOT_BRIGHTNESS; 
        break;
      case 1: 
        color1.G = RGB_FW_DOT_BRIGHTNESS; 
        break;
      case 2: 
        color1.B = RGB_FW_DOT_BRIGHTNESS; 
        break;
      case 3: 
        color1.R = color1.G = RGB_FW_DOT_BRIGHTNESS; 
        break;
      case 4: 
        color1.R = color1.B = RGB_FW_DOT_BRIGHTNESS; 
        break;
      case 5: 
        color1.G = color1.B = RGB_FW_DOT_BRIGHTNESS; 
        break;
    }
      
    var3 = 10;
    return var2; 

  // ����� ������� � ����������
  case 1:
    return RGB_FW_FLASH_RADIUS; 
  // ��������� �������
  case 2:
    return 256 + 64; 
  default:
    return 50;
  }
}
//==============================================================================


//==============================================================================
// �������, ����������� ��������� ��� ��������. ���������� ������ �� ���������� ����
//==============================================================================
int16_t rgb_firework_tick(uint8_t step)
{
  uint8_t Val;

  switch(step)
  {
  // ����� ������� �����
  case 0:
    ledstrip_add_3color(DotPos++, color1.R, color1.G, color1.B);
    ledstrip_fade_out_all(RGB_FW_DOT_BRIGHTNESS / RGB_FW_DOTSNUM_FADING);

    // ������� ��������� ��������
    var3 += RGB_FW_DOT_SLOWDOWN;
    
    return var3;
  // ����� ������� � ����������
  case 1:
    // ������� ����������� �����
    ledstrip_add_3color(DotPos + var1, RGB_FW_FLASH_BRIGHTNESS, RGB_FW_FLASH_BRIGHTNESS, RGB_FW_FLASH_BRIGHTNESS);
    // ������� ����������� �����
    ledstrip_add_3color(DotPos - var1, RGB_FW_FLASH_BRIGHTNESS, RGB_FW_FLASH_BRIGHTNESS, RGB_FW_FLASH_BRIGHTNESS);

    var1++;
    
    return RGB_FW_FLASH_PERIOD;
  // ��������� �������
  case 2:
    // ������ ��������� ���� ��� ��������� �������
    Val = DotPos - RGB_FW_FLASH_RADIUS + 1 + (rand() % ((RGB_FW_FLASH_RADIUS * 2) - 1));
    if (rand() & 1)
      ledstrip_add_3color(Val, RGB_FW_NOISE_STEP, RGB_FW_NOISE_STEP, RGB_FW_NOISE_STEP);
    else
      ledstrip_dec_3color(Val, RGB_FW_NOISE_STEP, RGB_FW_NOISE_STEP, RGB_FW_NOISE_STEP);

    // ������� ���������� ���� �����������
    ledstrip_fade_out_all(RGB_FW_FADEOUT_STEP);
    
    return RGB_FW_FADEOUT_PERIOD;
  }
  
  return -1;
}
//==============================================================================
#endif