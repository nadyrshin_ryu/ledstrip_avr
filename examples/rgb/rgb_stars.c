//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include "examples.h"
#include "rgb_stars.h"
#include <stdlib.h>

#if (LEDSTRIP_COLORSNUM == 3)

// �������� ������ ����������
static uint16_t DotPos;
static tRGB color1, color2;
     
//==============================================================================
// ������� �������� ����� ������. ���������� ���-�� ������ � ����� �������.
//==============================================================================
uint16_t rgb_stars_start(uint8_t step)
{
  // �������� ����� ������� ������, �������� ���� ��� ����������

  // ������� ������
  DotPos = rand() % LEDSTRIP_LEDNUM;
  // �������� ��������� ����
  color1.R = 255;
  color1.G = 255;
  color1.B = 255;

  color2.R = color2.G = color2.B = 0;
  
  return (256 / RGB_ST_ADD_STEP) + 1; 
}
//==============================================================================


//==============================================================================
// �������, ����������� ��������� ��� ��������. ���������� ������ �� ���������� ����
//==============================================================================
int16_t rgb_stars_tick(uint8_t step)
{
  // �������� ����� ������� ������, �������� ���� ��� ����������

  StepChangeColor((uint8_t *)&color2, (uint8_t *)&color1, RGB_ST_ADD_STEP);

  ledstrip_set_3color(DotPos, color2.R, color2.G, color2.B);
    
  // ����� ��� ����������
  ledstrip_fade_out_all(RGB_ST_DEC_STEP);

  return RGB_ST_PERIOD;
}
//==============================================================================
#endif