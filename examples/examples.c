//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include <delay.h>
#include "examples.h"


     
//==============================================================================
// ��������� � �������� ����� �������� �������� *source � �������� *desc
//==============================================================================
void StepChange(uint8_t *desc, uint8_t *source, uint8_t Step)
{
  if (*desc < *source)
  {
    if ((0xFF - *desc) < Step)
      *desc = 0xFF;
    else
      *desc += Step;
  }
    
  if (*desc > *source)
  {
    if (*desc > Step)
      *desc -= Step;
    else
      *desc = 0;
  }
}       
//==============================================================================


//==============================================================================
// ��������� � �������� ����� ����������� ���� pDesc � pSource
//==============================================================================
void StepChangeColor(uint8_t *pDesc, uint8_t *pSource, uint8_t Step)
{
  uint8_t ColorsNum = LEDSTRIP_COLORSNUM;
  while (ColorsNum--)
  {
    StepChange(pDesc, pSource, Step);
    pDesc++;
    pSource++;
  }
}
//==============================================================================

//==============================================================================
// ��������� �������� ���� *pSource � ���� *pDesc
//==============================================================================
void CopyColor(uint8_t *pDesc, uint8_t *pSource)
{
  uint8_t ColorsNum = LEDSTRIP_COLORSNUM;
  while (ColorsNum--)
  {
    *pDesc = *pSource;
    pDesc++;
    pSource++;
  }
}
//==============================================================================
