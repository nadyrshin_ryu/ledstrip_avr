//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include <delay.h>
#include "examples.h"
#include "rgbw_test.h"
#include <stdlib.h>

#if (LEDSTRIP_COLORSNUM == 4)

static uint16_t var1;
     
//==============================================================================
// ������� �������� ����� ������. ���������� ���-�� ������ � ����� �������.
//==============================================================================
uint16_t rgbw_test_start(uint8_t step)
{
  var1 = 0;
  return LEDSTRIP_LEDNUM*2; 
}
//==============================================================================


//==============================================================================
// �������, ����������� ��������� ��� ��������. ���������� ������ �� ���������� ����
//==============================================================================
int16_t rgbw_test_tick(uint8_t step)
{
  switch(step)
  {
  // ������� ������� ����� � ������� fade-out
  case 0:
    ledstrip_set_4color(var1 / 2, 255, 0, 0, 0);
    ledstrip_fade_out_all(RGBW_TST_FADEOUT_STEP);
    
    if (++var1 >= (LEDSTRIP_LEDNUM * 2))
      var1 = 0;
    return RGBW_TST_PERIOD;
  // ������ ������� ����� � ������� fade-out
  case 1:
    ledstrip_set_4color(var1 / 2, 0, 255, 0, 0);
    ledstrip_fade_out_all(RGBW_TST_FADEOUT_STEP);
    
    if (++var1 >= (LEDSTRIP_LEDNUM*2))
      var1 = 0;
    return RGBW_TST_PERIOD;
  // ����� ������� ����� � ������� fade-out
  case 2:
    ledstrip_set_4color(var1 / 2, 0, 0, 255, 0);
    ledstrip_fade_out_all(RGBW_TST_FADEOUT_STEP);
    
    if (++var1 >= (LEDSTRIP_LEDNUM*2))
      var1 = 0;
    return RGBW_TST_PERIOD;
  // ����� ������� ����� (������ R+G+B) � ������� fade-out
  case 3:
    ledstrip_set_4color(var1 / 2, 255, 255, 255, 0);
    ledstrip_fade_out_all(RGBW_TST_FADEOUT_STEP);
    
    if (++var1 >= (LEDSTRIP_LEDNUM*2))
      var1 = 0;
    return RGBW_TST_PERIOD;
  // ����� ������� ����� (����� W) � ������� fade-out
  case 4:
    ledstrip_set_4color(var1 / 2, 0, 0, 0, 255);
    ledstrip_fade_out_all(RGBW_TST_FADEOUT_STEP);
    
    if (++var1 >= (LEDSTRIP_LEDNUM*2))
      var1 = 0;
    return RGBW_TST_PERIOD;
  }
  
  return -1;
}
//==============================================================================
#endif