//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RGBW_TEST_H
#define _RGBW_TEST_H

#include "..\types.h"

#define RGBW_TST_STEP_NUM        5

// ������ �������� � ��
#define RGBW_TST_PERIOD         20
// ��� �������� ������� �����������
#define RGBW_TST_FADEOUT_STEP   4


uint16_t rgbw_test_start(uint8_t EffectNum);
int16_t rgbw_test_tick(uint8_t EffectNum);

#endif