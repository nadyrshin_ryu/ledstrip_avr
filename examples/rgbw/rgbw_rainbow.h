//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RGBW_RAINBOW_H
#define _RGBW_RAINBOW_H

#include "..\types.h"


//// ��������� �������� ������
// ���-�� ����� �� ������� ����� 2 ������� ������
#define RGBW_RB_1COLOR_STEPS    32
// ������ ���� �� �������� �� ������ ����� � �������
#define RGBW_RB_FADE_STEP       8
// ������ ����� ������ � ��
#define RGBW_RB_PERIOD          30


uint16_t rgbw_rainbow_start(uint8_t step);
int16_t rgbw_rainbow_tick(uint8_t step);

#endif