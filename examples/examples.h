//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _EXAMPLES_H
#define _EXAMPLES_H

#include <types.h>

#if (LEDSTRIP_COLORSNUM == 4)
#include "rgbw/rgbw_firework.h"
#include "rgbw/rgbw_stars.h"
#include "rgbw/rgbw_rainbow.h"
#include "rgbw/rgbw_test.h"
#endif

#if (LEDSTRIP_COLORSNUM == 3)
#include "rgb/rgb_firework.h"
#include "rgb/rgb_stars.h"
#include "rgb/rgb_rainbow.h"
#include "rgb/rgb_test.h"
#endif

typedef struct
{
  uint8_t G;
  uint8_t R;
  uint8_t B;
  uint8_t W;
} tRGBW;

typedef struct
{
  uint8_t G;
  uint8_t R;
  uint8_t B;
} tRGB;


// ��������� � �������� ����� �������� �������� *source � �������� *desc
void StepChange(uint8_t *desc, uint8_t *source, uint8_t Step);
// ��������� � �������� ����� ����������� ���� pDesc � pSource
void StepChangeColor(uint8_t *pDesc, uint8_t *pSource, uint8_t Step);
// ��������� �������� ���� *pSource � ���� *pDesc
void CopyColor(uint8_t *pDesc, uint8_t *pSource);

#endif