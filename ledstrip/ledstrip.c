//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include <intrinsics.h>
#include <delay.h>
#include "ledstrip.h"

uint8_t ledstrip_buff[LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM];


// ������� ��� ������ � GPIO
#define LEDSTRIP_Data_HIGH()    LEDSTRIP_Data_Port |= LEDSTRIP_Data_Mask
#define LEDSTRIP_Data_LOW()     LEDSTRIP_Data_Port &= ~LEDSTRIP_Data_Mask
#define LEDSTRIP_Data_TOGGLE()  LEDSTRIP_Data_Port ^= LEDSTRIP_Data_Mask


//==============================================================================
// ��������� �������������� ��� ����������������
//==============================================================================
void ledstrip_init(void)
{
  LEDSTRIP_Data_DDR |= LEDSTRIP_Data_Mask;
  LEDSTRIP_Data_Port |= LEDSTRIP_Data_Mask;
}
//==============================================================================


//==============================================================================
// ������� ���������� � uint8 �������� ������ uint8 ��������, ��������� ������
// ��� ������������ 
//==============================================================================
uint8_t Add2uint8val(uint8_t Val, uint8_t AddVal)
{
  if (((uint16_t) Val) + AddVal > 255)
    Val = 255;
  else
    Val += AddVal;
  
  return Val;
}
//==============================================================================

//==============================================================================
// ������� ��������� Val �� DecVal, ���������� �������� >= 0
//==============================================================================
uint8_t Dec2uint8val(uint8_t Val, uint8_t DecVal)
{
  if (Val < DecVal)
    Val = 0;
  else
    Val -= DecVal;
  
  return Val;
}
//==============================================================================


#if (LEDSTRIP_COLORSNUM == 3)
//==============================================================================
// ��������� ������������� RGB-�������� � LED � �������� LED_Num (��� ����������� �� WS2812)
//==============================================================================
void ledstrip_set_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num++] = G;
  ledstrip_buff[LED_Num++] = R;
  ledstrip_buff[LED_Num++] = B;
}
//==============================================================================


//==============================================================================
// ��������� ��������� � ����� ����� ��������� ����
//==============================================================================
void ledstrip_add_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� ��������� ������ ��� ����������� ����������
//==============================================================================
void ledstrip_dec_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
}
//==============================================================================
#endif

#if (LEDSTRIP_COLORSNUM == 4)
//==============================================================================
// ��������� ������������� RGBW-�������� � LED � �������� LED_Num (��� ����������� �� SK6812)
//==============================================================================
void ledstrip_set_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num++] = G;
  ledstrip_buff[LED_Num++] = R;
  ledstrip_buff[LED_Num++] = B;
  ledstrip_buff[LED_Num++] = W;
}
//==============================================================================


//==============================================================================
// ��������� ��������� � ����� ����� ��������� ����
//==============================================================================
void ledstrip_add_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
  ledstrip_buff[LED_Num] = Add2uint8val(ledstrip_buff[LED_Num], W);
  LED_Num++;
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� ��������� ������ ��� ����������� ����������
//==============================================================================
void ledstrip_dec_4color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B, uint8_t W)
{
  if (LED_Num >= LEDSTRIP_LEDNUM)
    return;
  
  LED_Num *= LEDSTRIP_COLORSNUM;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], G);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], R);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], B);
  LED_Num++;
  ledstrip_buff[LED_Num] = Dec2uint8val(ledstrip_buff[LED_Num], W);
  LED_Num++;
}
//==============================================================================
#endif


//==============================================================================
// ��������� ��������� ������� ������ ���� ����������� ������� � �������� �����
//==============================================================================
void ws2812_fade_in_all(uint16_t fade_step)
{
  for (uint16_t i = 0; i < (LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM); i++)
  {
    if ((0xFF - ledstrip_buff[i]) < fade_step)
      ledstrip_buff[i] = 0xFF;
    else
      ledstrip_buff[i] += fade_step;
  }
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� ������� ���� ����������� ������� � �������� �����
//==============================================================================
void ledstrip_fade_out_all(uint16_t fade_step)
{
  for (uint16_t i = 0; i < (LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM); i++)
  {
    if (ledstrip_buff[i] > fade_step)
      ledstrip_buff[i] -= fade_step;
    else
      ledstrip_buff[i] = 0;
  }
}
//==============================================================================


//==============================================================================
// ��������� ����� ��� ���������� �������
//==============================================================================
void ledstrip_all_off(void)
{
  for (uint16_t i = 0; i < (LEDSTRIP_LEDNUM * LEDSTRIP_COLORSNUM); i++)
    ledstrip_buff[i] = 0;
}
//==============================================================================


//==============================================================================
// ��������� ��������� ����� ��� ws2812 � ���������� � ��� ������ ���� 
//==============================================================================
#pragma optimize=size none
void ledstrip_sendarray(uint8_t *data, uint16_t datalen)
{
  uint8_t bitmask;
  __disable_interrupt();  

  LEDSTRIP_Data_LOW();
  delay_us(80);

  while (datalen--) 
  {
    bitmask = 0x80;
    
    while (bitmask)
    {
      if (*data & bitmask)
      {
        LEDSTRIP_Data_HIGH();
        bitmask >>= 1;
        __delay_cycles(2);
        LEDSTRIP_Data_LOW();
      }
      else
      {
        LEDSTRIP_Data_HIGH();
        LEDSTRIP_Data_LOW();
        bitmask >>= 1;
      }
    }
    data++;
  }
  
  LEDSTRIP_Data_HIGH();
  __enable_interrupt();  
}
//==============================================================================


//==============================================================================
// ��������� ��������� ��������� ������� �����������
//==============================================================================
void ledstrip_update(void)
{
  ledstrip_sendarray(ledstrip_buff, sizeof(ledstrip_buff));
}
//==============================================================================
