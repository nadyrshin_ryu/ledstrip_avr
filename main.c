//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <ioavr.h>
#include <inavr.h>
#include <ledstrip.h>
#include "main.h"
#include "rgb.h"
#include "rgbw.h"


void main( void )
{
  ledstrip_init();

  // �������������� ��������� ��������� �����
  srand(5);

  while (1)
  {
    /*
    for (uint8_t i=0; i < 10; i++)
      ledstrip_set_3color(i, 255, 0, 0);
    ledstrip_update();
*/

#if (LEDSTRIP_COLORSNUM == 4)
    rgbw_effects();
#endif

#if (LEDSTRIP_COLORSNUM == 3)
    rgb_effects();
#endif

  }
}
